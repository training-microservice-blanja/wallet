package com.metraplasa.microservice.wallet.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletController {

	@PreAuthorize("hasAuthority('SCOPE_review_transaksi')")
	@GetMapping("/wallet")
	public Map<String, Object> userWallet(Authentication authentication) {
		Map<String, Object> walletInfo = new HashMap<>();
		walletInfo.put("Current User", authentication.getPrincipal());
		walletInfo.put("Permissions", authentication.getAuthorities());
		return walletInfo;
	}
}
